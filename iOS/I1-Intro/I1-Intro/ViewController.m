//
//  ViewController.m
//  I1-Intro
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"A minha app arrancou...");
    
    _label1.text = @"Agora o texto é outro!";
    
}

- (IBAction)clickedButton:(id)sender {
    
    _label2.text = @"YAY FUNCIONA!!!!";
    
}

@end
