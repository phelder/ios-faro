//
//  SecondViewController.m
//  I15-PushNavigation
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelNome;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.nomeAmigo) {
        self.labelNome.text = [NSString stringWithFormat:@"O nome do amigo é: %@", self.nomeAmigo];
        
        self.navigationItem.title = self.nomeAmigo;
    } else {
    
        self.labelNome.text = @"Nenhum amigo escolhido!";
    
    }
    
    
}


@end
