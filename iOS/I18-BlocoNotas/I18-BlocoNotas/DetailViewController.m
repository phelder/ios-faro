//
//  DetailViewController.m
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "NotasDataSource.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *tfTitulo;
@property (weak, nonatomic) IBOutlet UITextView *tvDescricao;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // vamos pintar a border da textView Descrição
    
    self.tvDescricao.layer.borderWidth = 0.5;
    self.tvDescricao.layer.borderColor = [UIColor colorWithWhite:0.85 alpha:1].CGColor;
    self.tvDescricao.layer.cornerRadius = 7;
    
    if (self.nota == nil) { // NOVO
    
        self.navigationItem.title = @"Nova Nota";
        
    } else { // EDITAR
        
        self.navigationItem.title = @"Editar nota";
    
        self.tfTitulo.text = self.nota.titulo;
        self.tvDescricao.text = self.nota.descricao;
    }
}

- (IBAction)clickedGuardar:(id)sender {
    
    NotasDataSource *nds = [NotasDataSource sharedInstance];
    
    if (self.nota == nil) {
    
        Nota *novaNota = [Nota notaWithTitulo:self.tfTitulo.text descricao:self.tvDescricao.text];
        [nds.notas addObject:novaNota];
    
    } else {
        self.nota.titulo = self.tfTitulo.text;
        self.nota.descricao = self.tvDescricao.text;
    }
   
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
