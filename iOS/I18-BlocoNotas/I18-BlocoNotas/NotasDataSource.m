//
//  NotasDataSource.m
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "NotasDataSource.h"

@implementation NotasDataSource

+ (instancetype)sharedInstance {
    
    static NotasDataSource *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        instance = [[NotasDataSource alloc] init];
        
        instance.notas = [[NSMutableArray alloc] init];
        
    });
    
    return instance;
}

@end
