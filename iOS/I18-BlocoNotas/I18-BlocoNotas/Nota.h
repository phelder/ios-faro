//
//  Nota.h
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Nota : NSObject

@property (strong, nonatomic) NSString *titulo;
@property (strong, nonatomic) NSString *descricao;

- (instancetype)initWithTitulo:(NSString *)titulo descricao:(NSString *)descricao;

+ (instancetype)notaWithTitulo:(NSString *)titulo descricao:(NSString *)descricao;

@end
