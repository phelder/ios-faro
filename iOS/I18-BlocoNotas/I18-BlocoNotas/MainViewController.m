//
//  MainViewController.m
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "NotasDataSource.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewNotas;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    // SO PARA TESTES...
    NotasDataSource *nds = [NotasDataSource sharedInstance];
    
    [nds.notas addObject:[Nota notaWithTitulo:@"nota 1" descricao:@"desc nota 1"]];
    [nds.notas addObject:[Nota notaWithTitulo:@"nota 2" descricao:@"desc nota 2"]];
    [nds.notas addObject:[Nota notaWithTitulo:@"nota 3" descricao:@"desc nota 3"]];
    
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.tableViewNotas reloadData];
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NotasDataSource *nds = [NotasDataSource sharedInstance];
    
    return nds.notas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    NotasDataSource *nds = [NotasDataSource sharedInstance];
    Nota *nota = nds.notas[indexPath.row];
    
    cell.textLabel.text = nota.titulo;
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NotasDataSource *nds = [NotasDataSource sharedInstance];
    
    [self performSegueWithIdentifier:@"MainToDetail" sender:nds.notas[indexPath.row]];
    
}

#pragma mark - UIButton Actions

- (IBAction)clickedNovaNota:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToDetail" sender:nil];
    
}

 #pragma mark - Navigation
 

 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
     
     if ([segue.identifier isEqualToString:@"MainToDetail"]) {
         
         DetailViewController *dvc = segue.destinationViewController;
         dvc.nota = sender;
     
     }
 
 }


@end
