//
//  DetailViewController.h
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Nota;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Nota *nota;

@end
