//
//  NotasDataSource.h
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Nota.h"

@interface NotasDataSource : NSObject

@property (strong, nonatomic) NSMutableArray <Nota *> *notas;

+ (instancetype)sharedInstance;

@end
