//
//  Nota.m
//  I18-BlocoNotas
//
//  Created by Helder Pereira on 26/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Nota.h"

@implementation Nota

- (instancetype)initWithTitulo:(NSString *)titulo descricao:(NSString *)descricao
{
    self = [super init];
    if (self) {
        
        self.titulo = titulo;
        self.descricao = descricao;
        
    }
    return self;
}

+ (instancetype)notaWithTitulo:(NSString *)titulo descricao:(NSString *)descricao
{
    return [[Nota alloc] initWithTitulo:titulo descricao:descricao];
    
}

@end
