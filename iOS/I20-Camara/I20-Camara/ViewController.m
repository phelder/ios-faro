//
//  ViewController.m
//  I20-Camara
//
//  Created by Helder Pereira on 29/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;
@property (weak, nonatomic) IBOutlet UIButton *buttonTakePicture;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        self.buttonTakePicture.enabled = NO;
        
    }
}

- (IBAction)clickedTakePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    picker.allowsEditing = YES;
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)clickedChoosePicture:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.allowsEditing = YES;
    
    picker.delegate = self;
    
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    [self dismissViewControllerAnimated:YES completion:nil];
    
    self.imageView1.image = info[UIImagePickerControllerEditedImage];
    
    NSLog(@"%@", info);
    
}

- (IBAction)clickedLocalPicture:(id)sender {
    
    self.imageView1.image = [UIImage imageNamed:@"Homer"];
    
}

- (IBAction)clickedWriteToDisk:(id)sender {
    
    
    NSData *pngImageData = UIImagePNGRepresentation(self.imageView1.image);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *newFileURL = [docsURL URLByAppendingPathComponent:@"image1.png"];
    
    [pngImageData writeToURL:newFileURL atomically:YES];
    
}

- (IBAction)clickedReadFromDisk:(id)sender {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *loadFileURL = [docsURL URLByAppendingPathComponent:@"image1.png"];
    
//    NSData *imageData = [NSData dataWithContentsOfURL:loadFileURL];
//    self.imageView1.image = [UIImage imageWithData:imageData];
    
    self.imageView1.image = [UIImage imageWithContentsOfFile:loadFileURL.path];
    
}

- (IBAction)clickedWriteUserDefaults:(id)sender {
    
    NSData *pngImageData = UIImagePNGRepresentation(self.imageView1.image);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:pngImageData forKey:@"img1"];
    
    [defaults synchronize];
}

- (IBAction)clickedReadUserDefaults:(id)sender {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSData *imageData = [defaults objectForKey:@"img1"];
    
    self.imageView1.image = [UIImage imageWithData:imageData];
    
}

@end
