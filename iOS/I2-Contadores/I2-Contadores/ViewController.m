//
//  ViewController.m
//  I2-Contadores
//
//  Created by Helder Pereira on 27/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentContadores;

@end

@implementation ViewController
{
    int _contador1;
    int _contador2;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self actualizaLabels];
}

- (IBAction)clickedMais:(id)sender {
    
    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1++;
    } else { // SENAO E UM SO PODE SER O OUTRO...
        _contador2++;
    }
    [self actualizaLabels];
}

- (IBAction)clickedMenos:(id)sender {
    
    if (self.segmentContadores.selectedSegmentIndex == 0) {
        _contador1--;
    } else {
        _contador2--;
    }
    [self actualizaLabels];
}

- (void)actualizaLabels {
    self.label1.text = [NSString stringWithFormat:@"Contador 1: %d", _contador1];
    self.label2.text = [NSString stringWithFormat:@"Contador 2: %d", _contador2];
}

@end
