//
//  MySingleton.m
//  I17-Singletons
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MySingleton.h"

@implementation MySingleton

+ (instancetype)sharedInstance {

//    static MySingleton *instance = nil;
//    
//    if (instance == nil) {
//        
//        instance = [[MySingleton alloc] init];
//        
//    }
//    return instance;
    
    static MySingleton *instance = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        instance = [[MySingleton alloc] init];
        
    });
    
    return instance;
}

@end
