//
//  MySingleton.h
//  I17-Singletons
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MySingleton : NSObject

@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSArray *cenas;
@property (strong, nonatomic) NSNumber *numero;

@property (strong, nonatomic) NSString *textoPartilhado;

+ (instancetype)sharedInstance;

@end
