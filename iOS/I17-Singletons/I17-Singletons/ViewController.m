//
//  ViewController.m
//  I17-Singletons
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "MySingleton.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MySingleton *ms = [MySingleton sharedInstance]; // #348756
    
    ms.nome = @"Tobias";
    ms.numero = @(30);
    ms.cenas = @[
                 @"cena 1",
                 @"cena 2"
                 ];
}

- (void)viewDidAppear:(BOOL)animated {

    MySingleton *ms = [MySingleton sharedInstance];
    
    self.textField1.text = ms.textoPartilhado;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSLog(@"WILL DISAPPEAR");
    
    MySingleton *ms = [MySingleton sharedInstance];
    
    ms.textoPartilhado = self.textField1.text;
}



@end
