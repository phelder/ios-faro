//
//  SecondViewController.m
//  I17-Singletons
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"
#import "MySingleton.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField2;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MySingleton *ms = [MySingleton sharedInstance];
    
    NSLog(@"NOME: %@", ms.nome);
    NSLog(@"NUM: %@", ms.numero);
    NSLog(@"CENAS: %@", ms.cenas);
    
    self.textField2.text = ms.textoPartilhado;
    
    NSLog(@"2: DID LOAD");
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"2: WILL APPEAR");
}

- (void)viewDidAppear:(BOOL)animated {
    
    MySingleton *ms = [MySingleton sharedInstance];
    
    self.textField2.text = ms.textoPartilhado;

    NSLog(@"2: DID APPEAR");
}

- (void)viewWillDisappear:(BOOL)animated {
    
    NSLog(@"WILL DISAPPEAR");
    
    MySingleton *ms = [MySingleton sharedInstance];
    
    ms.textoPartilhado = self.textField2.text;
}

@end
