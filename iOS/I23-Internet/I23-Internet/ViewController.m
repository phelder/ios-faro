//
//  ViewController.m
//  I23-Internet
//
//  Created by Helder Pereira on 31/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadInternetTextContent];
}

- (IBAction)clickedLoadImage:(id)sender {
    
    // GCD - Grand Central Dispatch
    
    UIButton *clickedButton = sender;
    
    [clickedButton setTitle:@"Loading..." forState:UIControlStateNormal];
    clickedButton.enabled = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url = [NSURL URLWithString:@"http://cs.ucsb.edu/~zhelfinstein/simpsons/image.jpg"];
        
        NSData *imgData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView1.image = [UIImage imageWithData:imgData];
            [clickedButton setTitle:@"Load Image" forState:UIControlStateNormal];
            clickedButton.enabled = YES;
        });
        
    });
    
}

- (void)loadInternetTextContent {
    
    NSURL *url = [NSURL URLWithString:@"https://jsonplaceholder.typicode.com/users"];
    
    //NSString *string = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSData *jsonData = [NSData dataWithContentsOfURL:url];
    
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    
    for (int i = 0; i < jsonArray.count; i++) {
    
        NSString *n = jsonArray[i][@"name"];
        NSString *lat = jsonArray[i][@"address"][@"geo"][@"lat"];
        NSString *lon = jsonArray[i][@"address"][@"geo"][@"lng"];
        NSLog(@"NAME: %@", n);
        NSLog(@"COORDS: %@, %@", lat, lon);
    }
    
    
}


@end
