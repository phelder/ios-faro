//
//  DetailViewController.m
//  I28-OrdenarTabelas
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "Pessoa.h"
#import "AppDelegate.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonAdicionar;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.navigationItem.title = self.pessoa.nome;
 
    [self vericaSeExiste];
}

- (void)vericaSeExiste {

    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Favorito"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"webId = %@", self.pessoa.webId];
    
    [request setPredicate:predicate];
    
    NSArray *coredataResults = [context executeFetchRequest:request error:nil];
    
    if(coredataResults.count > 0) {
        self.buttonAdicionar.enabled = NO;
    }

}

- (IBAction)clickedAdicionarCoredata:(id)sender {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSManagedObject *fav = [NSEntityDescription insertNewObjectForEntityForName:@"Favorito" inManagedObjectContext:context];
    
    [fav setValue:self.pessoa.webId forKey:@"webId"];
    [fav setValue:self.pessoa.nome forKey:@"nome"];
    [fav setValue:self.pessoa.idade forKey:@"idade"];
    [fav setValue:self.pessoa.cidade forKey:@"cidade"];
    
    [appDelegate saveContext];
    
    self.buttonAdicionar.enabled = NO;
}

@end
