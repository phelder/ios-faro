//
//  DetailViewController.h
//  I28-OrdenarTabelas
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Pessoa;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Pessoa *pessoa;

@end
