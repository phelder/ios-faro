//
//  ViewController.m
//  I28-OrdenarTabelas
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"
#import "DetailViewController.h"
#import "AppDelegate.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentSort;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController
{
    NSArray<Pessoa *> *_malta;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _malta = @[
              [Pessoa pessoaWithWebId:@(1) nome:@"Tobias" idade:@(20) cidade:@"Porto"],
              [Pessoa pessoaWithWebId:@(2) nome:@"Tomás" idade:@(30) cidade:@"Lisboa"],
              [Pessoa pessoaWithWebId:@(3) nome:@"Marta" idade:@(60) cidade:@"Lisboa"],
              [Pessoa pessoaWithWebId:@(4) nome:@"Zeferino" idade:@(10) cidade:@"Lisboa"],
              [Pessoa pessoaWithWebId:@(5) nome:@"Sofia" idade:@(50) cidade:@"Porto"],
              [Pessoa pessoaWithWebId:@(6) nome:@"Zé" idade:@(70) cidade:@"Faro"],
              [Pessoa pessoaWithWebId:@(7) nome:@"Hugo" idade:@(10) cidade:@"Porto"],
              [Pessoa pessoaWithWebId:@(8) nome:@"Rui" idade:@(15) cidade:@"Faro"]
               ];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [self.tableView reloadData];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _malta.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Pessoa *p = _malta[indexPath.row];
    
    cell.textLabel.text = p.nome;
    cell.detailTextLabel.text = p.description;
    
    // VERIFICA COREDATA
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Favorito"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"webId = %@", p.webId];
    
    [request setPredicate:predicate];
    
    NSArray *coredataResults = [context executeFetchRequest:request error:nil];
    
    if(coredataResults.count > 0) {
        cell.backgroundColor = [UIColor greenColor];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;

}

- (IBAction)changedSegmentSort:(id)sender {
    
    NSSortDescriptor *descriptor;
    
    if (self.segmentSort.selectedSegmentIndex == 0) { // ORDERNAR POR ID
        
        descriptor = [NSSortDescriptor sortDescriptorWithKey:@"webId" ascending:YES];
        
    } else if (self.segmentSort.selectedSegmentIndex == 1) { // ORDENAR POR NOME
        
        descriptor = [NSSortDescriptor sortDescriptorWithKey:@"nome" ascending:YES];
        
    } else if (self.segmentSort.selectedSegmentIndex == 2) { // ORDENAR POR IDADE
        
        descriptor = [NSSortDescriptor sortDescriptorWithKey:@"idade" ascending:YES];
        
    } else if (self.segmentSort.selectedSegmentIndex == 3) { // ORDENAR POR CIDADE
        
        descriptor = [NSSortDescriptor sortDescriptorWithKey:@"cidade" ascending:YES];
        
    }
    
    _malta = [_malta sortedArrayUsingDescriptors:@[descriptor]];
    
    [self.tableView reloadData];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [self performSegueWithIdentifier:@"MainToDetail" sender:_malta[indexPath.row]];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    DetailViewController *dvc = segue.destinationViewController;
    dvc.pessoa = sender;
    
}

@end
