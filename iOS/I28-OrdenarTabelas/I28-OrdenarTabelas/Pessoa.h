//
//  Pessoa.h
//  I28-OrdenarTabelas
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSNumber *webId;
@property (strong, nonatomic) NSString *nome;
@property (strong, nonatomic) NSNumber *idade;
@property (strong, nonatomic) NSString *cidade;

- (instancetype)initWithWebId:(NSNumber *)webId nome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithWebId:(NSNumber *)webId nome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade;

@end
