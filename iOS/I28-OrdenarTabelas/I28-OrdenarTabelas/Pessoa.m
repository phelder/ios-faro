//
//  Pessoa.m
//  I28-OrdenarTabelas
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithWebId:(NSNumber *)webId nome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        self.webId = webId;
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithWebId:(NSNumber *)webId nome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade {
    
    return [[Pessoa alloc] initWithWebId:webId nome:nome idade:idade cidade:cidade];
    
}

- (NSString *)description {

    return [NSString stringWithFormat:@" %@ - %@ - %@ - %@", self.webId, self.nome, self.idade, self.cidade];
    
}

@end
