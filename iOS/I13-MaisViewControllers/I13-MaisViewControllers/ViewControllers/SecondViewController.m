//
//  SecondViewController.m
//  I13-MaisViewControllers
//
//  Created by Helder Pereira on 22/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"SECOND VIEW DID LOAD");
    NSLog(@"TEXTO: %@", self.texto1);
    
    self.label1.text = self.texto1;
}

- (IBAction)clickedBackButton:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
