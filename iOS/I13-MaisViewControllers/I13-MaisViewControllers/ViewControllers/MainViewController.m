//
//  MainViewController.m
//  I13-MaisViewControllers
//
//  Created by Helder Pereira on 22/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "SecondViewController.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)clickedGotoSecond:(id)sender {
    
    [self performSegueWithIdentifier:@"MainToSecond" sender:nil];
    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"SEGUE: %@", segue);
    NSLog(@"SENDER: %@", sender);
    NSLog(@"PREPARE FOR SEGUE");
    
    if ([segue.identifier isEqualToString:@"MainToSecond"]) {
        
        SecondViewController *svc =  segue.destinationViewController;

        //[svc setTexto1: [[self textField1] text]];
        svc.texto1 = self.textField1.text;
        
    }
    
}

@end
