//
//  ViewController.m
//  I21-GuardarFicheiros
//
//  Created by Helder Pereira on 29/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self guardarFicheiroTexto];
    [self lerFicheiroTexto];
}

- (void)lerFicheiroTexto {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *loadFileURL = [docsURL URLByAppendingPathComponent:@"ficheiro1.txt"];
    
    NSString *qqcoisa = [NSString stringWithContentsOfURL:loadFileURL encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"%@", qqcoisa);
}

- (void)guardarFicheiroTexto {

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSString *conteudo = @"ola eu sou a linha 1\nlinha 2\n\n\n\n\n\n\nFIM";

    NSURL *newFileURL = [docsURL URLByAppendingPathComponent:@"ficheiro1.txt"];
    
    [conteudo writeToURL:newFileURL atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    NSLog(@"%@", newFileURL);
    
}


@end
