//
//  ViewController.m
//  I25-Mapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapKitView;

@end

@implementation ViewController
{
    CLLocationManager *_manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _manager = [[CLLocationManager alloc] init];
    _manager.delegate = self;
    [_manager requestWhenInUseAuthorization];
}

#pragma mark - UIButton Actions

- (IBAction)clickedAddPin:(id)sender {
    
    //MKAnnotationView *novoPin = [[MKAnnotationView alloc] init];
    MKPointAnnotation *novoPin = [[MKPointAnnotation alloc] init];
//    CLLocationCoordinate2D coordenadas;
//    coordenadas.latitude = 1;
//    coordenadas.longitude = 2;
    novoPin.coordinate = CLLocationCoordinate2DMake(37.0194, -7.9304);
    novoPin.title = @"Faro";
    novoPin.subtitle = @"é mesmo...";
    
    [self.mapKitView addAnnotation:novoPin];
}

- (IBAction)clickedAddMyLocation:(id)sender {
    
    [_manager startUpdatingLocation];
    
}

#pragma mark - CLLocationManagerDelegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    [_manager stopUpdatingLocation];
    
    //NSLog(@"%@", locations);
    
    CLLocation *location = locations[0];
    
    MKPointAnnotation *novoPin = [[MKPointAnnotation alloc] init];
    
    novoPin.coordinate = location.coordinate;
    
    novoPin.title = @"Eu estou aqui!";
    novoPin.subtitle = @"Ou não...";
    
    [self.mapKitView addAnnotation:novoPin];
    
}

@end
