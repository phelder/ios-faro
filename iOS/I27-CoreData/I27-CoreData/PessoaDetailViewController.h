//
//  PessoaDetailViewController.h
//  I27-CoreData
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Pessoa;

@interface PessoaDetailViewController : UIViewController

@property (strong, nonatomic) Pessoa *pessoa;

@end
