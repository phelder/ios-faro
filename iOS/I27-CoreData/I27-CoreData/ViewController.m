//
//  ViewController.m
//  I27-CoreData
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"
#import "AppDelegate.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // FICOU AKI SO PARA EXEMPLO
    // FOI APAGADO DO STORYBOARD
    // JA NAO EXISTE ENTAO NA APP...
}

- (void)exemploSelect {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    // A mesma ideia que: SELECT * FROM Pessoa
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Pessoa"];
    
    NSArray<Pessoa *> *gente = [context executeFetchRequest:fetchRequest error:nil];
    
    for (int i = 0; i < gente.count; i++) {
    
        NSLog(@"%@", gente[i].nome);
        NSLog(@"%@", gente[i].cidade);
    
    }
}

- (void)insereRegistoPessoa {

    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    Pessoa *newPessoa = [NSEntityDescription insertNewObjectForEntityForName:@"Pessoa" inManagedObjectContext:context];
    
    newPessoa.nome = @"Tobias";
    newPessoa.idade = @(20);
    newPessoa.cidade = @"Porto";
    
    [appDelegate saveContext];
    
}


@end
