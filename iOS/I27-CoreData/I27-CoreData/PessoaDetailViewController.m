//
//  PessoaDetailViewController.m
//  I27-CoreData
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "PessoaDetailViewController.h"
#import "Pessoa.h"
#import "AppDelegate.h"

@interface PessoaDetailViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textFieldNome;
@property (weak, nonatomic) IBOutlet UITextField *textFieldIdade;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCidade;

@end

@implementation PessoaDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.pessoa) {
        
        self.textFieldNome.text = self.pessoa.nome;
        self.textFieldIdade.text = self.pessoa.idade.description;
        self.textFieldCidade.text = self.pessoa.cidade;
    }
    
}


#pragma mark - UIButton actions

- (IBAction)clickedGuardar:(id)sender {
    
    AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.managedObjectContext;
    
    if (self.pessoa == nil) {
        self.pessoa = [NSEntityDescription insertNewObjectForEntityForName:@"Pessoa" inManagedObjectContext:context];
    }
    
    self.pessoa.nome = self.textFieldNome.text;
    self.pessoa.idade = @(self.textFieldIdade.text.intValue);
    self.pessoa.cidade = self.textFieldCidade.text;
    
    [appDelegate saveContext];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
