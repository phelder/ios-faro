//
//  Pessoa+CoreDataProperties.h
//  I27-CoreData
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Pessoa.h"

NS_ASSUME_NONNULL_BEGIN

@interface Pessoa (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *nome;
@property (nullable, nonatomic, retain) NSString *cidade;
@property (nullable, nonatomic, retain) NSNumber *idade;

@end

NS_ASSUME_NONNULL_END
