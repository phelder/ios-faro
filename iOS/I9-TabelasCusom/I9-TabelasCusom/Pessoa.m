//
//  Pessoa.m
//  I9-TabelasCusom
//
//  Created by Helder Pereira on 19/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade {
    
    self = [super init];
    if (self) {
        
        self.nome = nome;
        self.idade = idade;
        self.cidade = cidade;
        
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(NSNumber *)idade cidade:(NSString *)cidade {

    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];

}

@end
