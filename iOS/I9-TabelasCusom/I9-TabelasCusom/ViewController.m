//
//  ViewController.m
//  I9-TabelasCusom
//
//  Created by Helder Pereira on 19/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Pessoa.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray <Pessoa *> *_gente;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _gente = @[
               [Pessoa pessoaWithNome:@"Zé" idade:@(34) cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Maria" idade:@(30) cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Fernando" idade:@(12) cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Lucas" idade:@(20) cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Marco" idade:@(40) cidade:@"Porto"],
               [Pessoa pessoaWithNome:@"Vanessa" idade:@(50) cidade:@"Braga"],
               [Pessoa pessoaWithNome:@"Rita" idade:@(32) cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Joana" idade:@(12) cidade:@"Braga"],
               [Pessoa pessoaWithNome:@"Fernando" idade:@(43) cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Tobias" idade:@(45) cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Tomás" idade:@(65) cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Tomé" idade:@(35) cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Marta" idade:@(75) cidade:@"Lisboa"],
               [Pessoa pessoaWithNome:@"Damien" idade:@(53) cidade:@"Faro"],
               [Pessoa pessoaWithNome:@"Nunes" idade:@(23) cidade:@"Lisboa"]
               ];
    
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _gente.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    if (indexPath.row % 2 == 0) {
        cell.backgroundColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    }
    
    UILabel *labelNome = [cell viewWithTag:1];
    UILabel *labelIdade = [cell viewWithTag:2];
    UILabel *labelCidade = [cell viewWithTag:3];
    
    Pessoa *p = _gente[indexPath.row];
    
    labelNome.text = p.nome;
    labelIdade.text = p.idade.description;
    labelCidade.text = p.cidade;
    
    return cell;
}



@end
