//
//  ViewController.m
//  I8-TabelasStoryboard
//
//  Created by Helder Pereira on 19/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation ViewController
{
    NSArray <NSDictionary *> * _malta;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _malta = @[
               @{
                   @"nome": @"Helder",
                   @"idade": @(35)
                },
               @{
                   @"nome": @"Tobias",
                   @"idade": @(20)
                   },
               @{
                   @"nome": @"Tomás",
                   @"idade": @(23)
                   },
               @{
                   @"nome": @"Tomé",
                   @"idade": @(40)
                   },
               @{
                   @"nome": @"Manuel",
                   @"idade": @(54)
                   },
               @{
                   @"nome": @"Rita",
                   @"idade": @(25)
                   }
               ];
}


#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _malta.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    NSDictionary *pessoa = _malta[indexPath.row];
    
    NSString *nome = pessoa[@"nome"];
    NSNumber *idade = pessoa[@"idade"];
    
    cell.textLabel.text = nome;
    cell.detailTextLabel.text = idade.description;
    
    return cell;
}

@end
