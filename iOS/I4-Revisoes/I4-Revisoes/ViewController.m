//
//  ViewController.m
//  I4-Revisoes
//
//  Created by Helder Pereira on 17/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;
@property (weak, nonatomic) IBOutlet UILabel *label3;

@end

@implementation ViewController
{
    int _contador1;
    int _contador2;
    int _contador3;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.label1.text = [NSString stringWithFormat:@"%d", 0];
    self.label2.text = [NSString stringWithFormat:@"%d", 0];
    self.label3.text = [NSString stringWithFormat:@"%d", 0];
    
}


- (IBAction)clickedButton1:(id)sender {
    _contador1++;
    self.label1.text = [NSString stringWithFormat:@"%d", _contador1];
}

- (IBAction)clickedButton2:(id)sender {
    _contador2++;
    self.label2.text = [NSString stringWithFormat:@"%d", _contador2];
}

- (IBAction)clickedButton3:(id)sender {
    _contador3++;
    self.label3.text = [NSString stringWithFormat:@"%d", _contador3];
}

@end
