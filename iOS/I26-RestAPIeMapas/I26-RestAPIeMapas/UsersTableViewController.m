//
//  UsersTableViewController.m
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "UsersTableViewController.h"
#import "AFNetworking.h"
#import "UsersDataSource.h"
#import "UserDetailViewController.h"

@interface UsersTableViewController ()

@end

@implementation UsersTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self downloadUsers];
}

- (void)downloadUsers {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"https://jsonplaceholder.typicode.com/users"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            [self parseResponse:responseObject];
            [self.tableView reloadData];
        }
    }];
    [dataTask resume];
}

- (void)parseResponse:(NSArray *)responseObject {
    
    NSMutableArray <User *> *users = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < responseObject.count; i++) {
    
        User *newUser = [[User alloc] init];
        newUser.webID = responseObject[i][@"id"];
        newUser.name = responseObject[i][@"name"];
        newUser.username = responseObject[i][@"username"];
        newUser.email = responseObject[i][@"email"];
        newUser.phone = responseObject[i][@"phone"];
        newUser.website = responseObject[i][@"website"];
        
        UserAddress *newAddress = [[UserAddress alloc] init];
        newAddress.street = responseObject[i][@"address"][@"street"];
        newAddress.suite = responseObject[i][@"address"][@"suite"];
        newAddress.city = responseObject[i][@"address"][@"city"];
        newAddress.zipcode = responseObject[i][@"address"][@"zipcode"];
        
        NSNumber *lat = responseObject[i][@"address"][@"geo"][@"lat"];
        NSNumber *lng = responseObject[i][@"address"][@"geo"][@"lng"];
        
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = lat.floatValue;
        coordinate.longitude = lng.floatValue;
        
        newAddress.geo = coordinate;
        
        newUser.address = newAddress;
        
        UserCompany *newCompany = [[UserCompany alloc] init];
        newCompany.name = responseObject[i][@"company"][@"name"];
        newCompany.catchPhrase = responseObject[i][@"company"][@"catchPhrase"];
        
        NSString *bs = responseObject[i][@"company"][@"bs"];
        
        newCompany.businesses = [bs componentsSeparatedByString:@" "];
    
        newUser.company = newCompany;
        
        [users addObject:newUser];
    }
    
    UsersDataSource *uds = [UsersDataSource sharedDataSource];
    uds.users = [NSArray arrayWithArray:users];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    UsersDataSource *uds = [UsersDataSource sharedDataSource];
    
    return uds.users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell" forIndexPath:indexPath];
    
    UsersDataSource *uds = [UsersDataSource sharedDataSource];
    
    User *user = uds.users[indexPath.row];
    
    cell.textLabel.text = user.name;
    cell.detailTextLabel.text = user.address.street;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UsersDataSource *uds = [UsersDataSource sharedDataSource];
    
    User *user = uds.users[indexPath.row];
    
    [self performSegueWithIdentifier:@"UsersTableToUserDetail" sender:user];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"UsersTableToUserDetail"]) {
        
        UserDetailViewController *udvc = segue.destinationViewController;
        
        udvc.user = sender;
        
    }
}


@end
