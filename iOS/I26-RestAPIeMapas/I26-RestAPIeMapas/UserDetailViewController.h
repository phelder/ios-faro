//
//  UserDetailViewController.h
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"

@interface UserDetailViewController : UIViewController

@property (strong, nonatomic) User *user;

@end
