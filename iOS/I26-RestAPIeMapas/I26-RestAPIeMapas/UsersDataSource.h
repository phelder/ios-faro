//
//  UsersDataSource.h
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface UsersDataSource : NSObject

@property (strong, nonnull) NSArray<User *> *users;

+ (instancetype)sharedDataSource;

@end
