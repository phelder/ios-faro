//
//  MapViewController.m
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "UsersDataSource.h"

@interface MapViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UsersDataSource *uds = [UsersDataSource sharedDataSource];
    
    for (User *user in uds.users) {
        
        MKPointAnnotation *novoPin = [[MKPointAnnotation alloc] init];
        novoPin.title = user.name;
        novoPin.subtitle = user.company.name;
        novoPin.coordinate = user.address.geo;
    
        [self.mapView addAnnotation:novoPin];
    }
    
    MKCoordinateRegion region;
    region.center = CLLocationCoordinate2DMake(0, 0);
    region.span = MKCoordinateSpanMake(100, 100);
    
    [self.mapView setRegion:region animated:YES];
}


@end
