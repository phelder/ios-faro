//
//  UserDetailViewController.m
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 05/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "UserDetailViewController.h"
#import <MapKit/MapKit.h>

@interface UserDetailViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITextView *textViewUserInfo;

@end

@implementation UserDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = self.user.name;

    NSMutableString *info = [[NSMutableString alloc] init];
    
    [info appendFormat:@"NOME: %@\n", self.user.name];
    [info appendFormat:@"USER: %@\n\n", self.user.username];
    [info appendFormat:@"MORADA: %@\n\n", self.user.address.street];
    [info appendFormat:@"EMPRESA: %@\n\n", self.user.company.name];
    
    NSString *bs = [self.user.company.businesses componentsJoinedByString:@",\n"];
    
    [info appendFormat:@"NEGOCIOS:\n%@", bs];
    
    self.textViewUserInfo.text = info;
    
    MKPointAnnotation *novoPin = [[MKPointAnnotation alloc] init];
    novoPin.title = self.user.name;
    novoPin.subtitle = self.user.company.name;
    novoPin.coordinate = self.user.address.geo;
    
    [self.mapView addAnnotation:novoPin];

    //[self.mapView setCenterCoordinate:self.user.address.geo];
    
    MKCoordinateRegion region;
    region.center = self.user.address.geo;
    region.span = MKCoordinateSpanMake(5, 5);
    
    [self.mapView setRegion:region animated:YES];
}

@end
