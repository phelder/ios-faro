//
//  UsersDataSource.m
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "UsersDataSource.h"

@implementation UsersDataSource

+ (instancetype)sharedDataSource {
    
    static UsersDataSource *instance;
    static dispatch_once_t onceToken;
 
    dispatch_once(&onceToken, ^{
        instance = [[UsersDataSource alloc] init];
    });
    
    return instance;
}

@end
