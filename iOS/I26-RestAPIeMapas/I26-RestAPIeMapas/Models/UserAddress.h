//
//  UserAddress.h
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface UserAddress : NSObject

@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *suite;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *zipcode;
@property (assign, nonatomic) CLLocationCoordinate2D geo;

@end
