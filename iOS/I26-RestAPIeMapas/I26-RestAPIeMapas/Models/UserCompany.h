//
//  UserCompany.h
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserCompany : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *catchPhrase;
@property (strong, nonatomic) NSArray<NSString *> *businesses;

@end
