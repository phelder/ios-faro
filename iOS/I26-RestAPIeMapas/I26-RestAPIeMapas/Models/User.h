//
//  User.h
//  I26-RestAPIeMapas
//
//  Created by Helder Pereira on 02/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAddress.h"
#import "UserCompany.h"

@interface User : NSObject

@property (strong, nonnull) NSNumber *webID;
@property (strong, nonnull) NSString *name;
@property (strong, nonnull) NSString *username;
@property (strong, nonnull) NSString *email;
@property (strong, nonnull) UserAddress *address;
@property (strong, nonnull) NSString *phone;
@property (strong, nonnull) NSString *website;
@property (strong, nonnull) UserCompany *company;

@end
