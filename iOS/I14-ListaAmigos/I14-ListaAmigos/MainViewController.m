//
//  MainViewController.m
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation MainViewController
{
    NSArray <NSString *> *_amigos;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _amigos = @[
                @"Tomás",
                @"Nuno",
                @"Tomé",
                @"Rita",
                @"Diogo",
                @"Ruben",
                @"Joana",
                @"Zé",
                @"Fernando",
                @"Xico",
                @"Carlos",
                @"Carla",
                @"Tiago",
                @"Marta",
                @"Bruno",
                @"Sérgio"
                ];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _amigos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    cell.textLabel.text = _amigos[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"MainToDetail" sender:indexPath];
    
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MainToDetail"]) {
    
        DetailViewController *dvc = segue.destinationViewController;
        
        NSIndexPath *indexPath = sender;
        
        dvc.nomeAmigo = _amigos[indexPath.row];
        
    }
    
}


@end
