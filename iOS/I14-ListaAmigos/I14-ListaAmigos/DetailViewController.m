//
//  DetailViewController.m
//  I14-ListaAmigos
//
//  Created by Helder Pereira on 24/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelDetail;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.labelDetail.text = self.nomeAmigo;
}

- (IBAction)clickedBack:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
