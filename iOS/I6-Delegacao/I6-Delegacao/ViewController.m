//
//  ViewController.m
//  I6-Delegacao
//
//  Created by Helder Pereira on 17/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelYes;
@property (weak, nonatomic) IBOutlet UILabel *labelNo;
@property (weak, nonatomic) IBOutlet UILabel *labelMaybe;

@end

@implementation ViewController
{
    int _contadorYes;
    int _contadorNo;
    int _contadorMaybe;
}


- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UIButton Actions

- (IBAction)clickedMostraAlerta:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] init];
    
    alert.title = @"O meu alerta!!!";
    alert.message = @"uma mensagem de alerta";
    
    [alert addButtonWithTitle:@"YES"];
    [alert addButtonWithTitle:@"NO"];
    [alert addButtonWithTitle:@"MAYBE"];
    
    alert.cancelButtonIndex = 1;
    
    alert.delegate = self;
    
    [alert show];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
    
        _contadorYes++;
        self.labelYes.text = [NSString stringWithFormat:@"YES: %d", _contadorYes];
        
    }

    if (buttonIndex == 1) {
        _contadorNo++;
        self.labelNo.text = [NSString stringWithFormat:@"NO: %d", _contadorNo];
    }
    
    if (buttonIndex == 2) {
        _contadorMaybe++;
        self.labelMaybe.text = [NSString stringWithFormat:@"MAYBE: %d", _contadorMaybe];
    }
}

@end
