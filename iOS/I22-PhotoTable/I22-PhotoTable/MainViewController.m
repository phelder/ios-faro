//
//  MainViewController.m
//  I22-PhotoTable
//
//  Created by Helder Pereira on 31/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewPhotos;

@end

@implementation MainViewController
{
    NSMutableArray <UIImage *> *_images;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _images = [[NSMutableArray alloc] init];
    
    [self addFolderImagesToArray];
}

- (IBAction)clickedAddPhoto:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    picker.delegate = self;
    
    picker.allowsEditing = YES;
    
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _images.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    UIImageView *ivPhoto = [cell viewWithTag:1];
    
    ivPhoto.image = _images[indexPath.row];
    
    return cell;
}

#pragma mark - UIImagePickerControllerDelegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {

    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *img = info[UIImagePickerControllerEditedImage];
    
    [_images addObject:img];
    [self saveToDiskImage:img];
    
    [self.tableViewPhotos reloadData];
}

- (void)saveToDiskImage:(UIImage *)image {

    NSData *pngImageData = UIImagePNGRepresentation(image);
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *imagesURL = [docsURL URLByAppendingPathComponent:@"imgs"];
    
    [fileManager createDirectoryAtURL:imagesURL withIntermediateDirectories:YES attributes:nil error:nil];
    
    NSDate *now = [[NSDate alloc] init];
    NSString *imageName = [NSString stringWithFormat:@"%@.png", now.description];
    
    NSURL *newImageURL = [imagesURL URLByAppendingPathComponent:imageName];
    
    [pngImageData writeToURL:newImageURL atomically:YES];
}

- (void)addFolderImagesToArray {

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *docsURL = [fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask][0];

    NSURL *imagesURL = [docsURL URLByAppendingPathComponent:@"imgs"];
    
    NSArray *contents = [fileManager contentsOfDirectoryAtURL:imagesURL includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:nil];
    
    for (NSURL *url in contents) {
        
        UIImage *image = [UIImage imageWithContentsOfFile:url.path];
        
        if (image) {
            [_images addObject:image];
        }
        
    }

}

@end
