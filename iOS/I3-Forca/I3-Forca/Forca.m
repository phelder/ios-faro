//
//  Forca.m
//  O15-Forca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Forca.h"

@implementation Forca
{
    NSString *_fraseSecreta;
    NSMutableString *_fraseJogo;
    int _vidas;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:@"Qualquer coisa"];
    }
    return self;
}

- (instancetype)initWithFrase:(NSString *)frase
{
    self = [super init];
    if (self) {
        [self iniciaJogoComFrase:frase];
    }
    return self;
}

- (instancetype)initWithFrases:(NSArray<NSString *> *)frases
{
    self = [super init];
    if (self) {
        int pos = arc4random() % frases.count;
        
        [self iniciaJogoComFrase:frases[pos]];
    }
    return self;
}

- (void)iniciaJogoComFrase:(NSString *)frase {
    
    _fraseSecreta = frase;
    _fraseJogo = [[NSMutableString alloc] init];
    _vidas = 6;
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        char letra = [_fraseSecreta characterAtIndex:i];
        if (letra == ' ') {
            [_fraseJogo appendString:@" "];
        } else {
            [_fraseJogo appendString:@"-"];
        }
        
    }
}

- (BOOL)verificaLetra:(NSString *)letra {
    
    BOOL acertou = NO;
    
    for (int i = 0; i < _fraseSecreta.length; i++) {
        
        NSRange range = NSMakeRange(i, 1);
        
        NSString *letraFrase = [_fraseSecreta substringWithRange:range];
        
        if ([letraFrase compare:letra options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch] == NSOrderedSame) {
            
            [_fraseJogo replaceCharactersInRange:range withString:letraFrase];
            acertou = YES;
        }
        
    }
    
    if (!acertou) {
        _vidas--;
    }

    return acertou;
}

- (NSString *)fraseJogo {
    
    return _fraseJogo.description;
    
}

- (int)vidas {
    return _vidas;
}

- (BOOL)emJogo {
    
    if (![_fraseSecreta isEqualToString:_fraseJogo] && _vidas > 0) {
        return YES;
    }
    return NO;
}

- (BOOL)venceu {
    if (!self.emJogo && _vidas > 0) {
        return YES;
    }
    return NO;
}


@end
