//
//  Forca.h
//  O15-Forca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Forca : NSObject

- (instancetype)initWithFrase:(NSString *)frase;
- (instancetype)initWithFrases:(NSArray<NSString *> *)frases;

- (BOOL)verificaLetra:(NSString *)letra;

- (NSString *)fraseJogo;
- (int)vidas;

- (BOOL)emJogo;
- (BOOL)venceu;

@end
