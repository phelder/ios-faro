//
//  ViewController.m
//  I3-Forca
//
//  Created by Helder Pereira on 29/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"
#import "Forca.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelVidas;
@property (weak, nonatomic) IBOutlet UILabel *labelFraseSecreta;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;

@property (weak, nonatomic) IBOutlet UIView *buttonParentView;

@end

@implementation ViewController
{
    Forca *_f1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _f1 = [[Forca alloc] init];
    
    self.labelStatus.text = nil;
    self.labelVidas.text = [NSString stringWithFormat:@"Vidas: %d", _f1.vidas];
    self.labelFraseSecreta.text = _f1.fraseJogo;
    
    
    
    for (UIButton *letterButton in self.buttonParentView.subviews) {
        
        letterButton.enabled = YES;
        
    }
    
}

- (IBAction)clickedLetterButton:(id)sender {
    
    if(![_f1 emJogo]) {
        return;
    }
    
    UIButton *letterButton = sender;
    NSString *letter = letterButton.currentTitle;
    
    [_f1 verificaLetra:letter];
    
    self.labelVidas.text = [NSString stringWithFormat:@"Vidas: %d", _f1.vidas];
    self.labelFraseSecreta.text = _f1.fraseJogo;
    
    if(![_f1 emJogo]) {
        if ([_f1 venceu]) {
            self.labelStatus.text = @"VENCEU";
        } else {
            self.labelStatus.text = @"NABO!!!!!";
        }
    }
}



@end
