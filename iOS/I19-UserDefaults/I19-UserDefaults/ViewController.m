//
//  ViewController.m
//  I19-UserDefaults
//
//  Created by Helder Pereira on 29/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField1;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    self.textField1.text = [defaults objectForKey:@"texto1"];
}

- (IBAction)clickedGuardar:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.textField1.text forKey:@"texto1"];
    
    [defaults synchronize];
}

- (void)exemploEscrever {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"Helder" forKey:@"nome"];
    [defaults setObject:@(35) forKey:@"idade"];
    
    [defaults synchronize];
}

- (void)exemploLer {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *textoLido = [defaults objectForKey:@"nome"];
    
    NSLog(@"%@", textoLido);
}

@end
