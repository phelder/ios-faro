//
//  ViewController.m
//  I5-Cores
//
//  Created by Helder Pereira on 17/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *quadradoCor;

@end

@implementation ViewController
{
    NSArray<UIColor *> *_cores;
    int _contador;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _cores = @[
               [UIColor colorWithRed:0/255.0 green:255/255.0 blue:128/255.0 alpha:1],
               [UIColor redColor],
               [UIColor greenColor],
               [UIColor blueColor],
               [UIColor yellowColor],
               [UIColor purpleColor],
               ];
    
    self.quadradoCor.backgroundColor = _cores[0];
}

- (IBAction)clickedMudaCor:(id)sender {
    
//      #00FF88
//      0, 255, 128
//
//    self.quadradoCor.backgroundColor = [UIColor colorWithRed:0/255.0 green:255/255.0 blue:128/255.0 alpha:1];
//    
//    if ([self.quadradoCor.backgroundColor isEqual:[UIColor redColor]]) {
//        
//        self.quadradoCor.backgroundColor = [UIColor greenColor];
//        
//    } else if ([self.quadradoCor.backgroundColor isEqual:[UIColor greenColor]]) {
//    
//        self.quadradoCor.backgroundColor = [UIColor blueColor];
//        
//    } else if ([self.quadradoCor.backgroundColor isEqual:[UIColor blueColor]]) {
//    
//        self.quadradoCor.backgroundColor = [UIColor redColor];
//    }
    
    _contador++;
    
    if (_contador == _cores.count) {
        _contador = 0;
    }
    
    self.quadradoCor.backgroundColor = _cores[_contador];
    
}

@end
