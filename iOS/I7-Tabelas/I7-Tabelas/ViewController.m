//
//  ViewController.m
//  I7-Tabelas
//
//  Created by Helder Pereira on 17/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView1;

@end

@implementation ViewController
{
    NSArray <NSString *> *_nomes;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nomes = @[
               @"Tio Patinhas",
               @"Donald",
               @"Gastão",
               @"Huginho",
               @"Zézinho",
               @"Luísinho",
               @"Prof. Pardal"
               ];
    
    self.tableView1.dataSource = self;
    self.tableView1.delegate = self;
    
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _nomes.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recycleCell"];
    
    if (cell == nil) {
        // NAO RECICLOU
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"recycleCell"];
        
        NSLog(@"CELULA NOVA");
        
    } else {
        // RECICLOU
        
        NSLog(@"CELULA RECICLADA");
    }

    cell.textLabel.text = _nomes[indexPath.row];
    
    return cell;
    
}

#pragma mark - UITableViewDelegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSLog(@"%@", _nomes[indexPath.row]);
    
    UIAlertView *av = [[UIAlertView alloc] init];
    
    av.title = @"Nome";
    av.message = _nomes[indexPath.row];
    
    [av addButtonWithTitle:@"OK"];
    
    av.cancelButtonIndex = 0;
    
    [av show];
}

@end
