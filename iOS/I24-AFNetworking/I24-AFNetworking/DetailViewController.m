//
//  DetailViewController.m
//  I24-AFNetworking
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "DetailViewController.h"
#import "UIImageView+AFNetworking.h"
#import "Song.h"

@interface DetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *ivBig;
@property (weak, nonatomic) IBOutlet UIImageView *ivThumb;
@property (weak, nonatomic) IBOutlet UILabel *labelArtist;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UITextView *tvLyrics;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.labelTitle.text = self.song.title;
    self.labelArtist.text = self.song.artist;
    self.tvLyrics.text = self.song.lyrics;
    
    [self.ivBig setImageWithURL:[NSURL URLWithString:self.song.thumbURL]];
    [self.ivThumb setImageWithURL:[NSURL URLWithString:self.song.thumbURL]];
}


@end
