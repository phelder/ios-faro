//
//  DetailViewController.h
//  I24-AFNetworking
//
//  Created by Helder Pereira on 19/09/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Song;

@interface DetailViewController : UIViewController

@property (strong, nonatomic) Song *song;

@end
