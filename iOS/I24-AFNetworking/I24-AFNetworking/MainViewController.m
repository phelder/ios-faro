//
//  MainViewController.m
//  I24-AFNetworking
//
//  Created by Helder Pereira on 31/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "MainViewController.h"
#import "AFNetworking/AFNetworking.h"
#import "UIKit+AFNetworking/UIImageView+AFNetworking.h"
#import "Song.h"
#import "DetailViewController.h"

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableViewSongs;

@end

@implementation MainViewController
{
    NSArray<Song *> *_music;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:@"http://reality6.com/musicservicejson.php"];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            //NSLog(@"%@ %@", response, responseObject);
            [self parseResponse:responseObject];
            [self.tableViewSongs reloadData];
        }
    }];
    [dataTask resume];
}

- (void)parseResponse:(NSArray *)responseObject {
    
    NSMutableArray *newSongs = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < responseObject.count; i++) {
        
        Song *s = [[Song alloc] init];
        s.webID = responseObject[i][@"id"];
        s.title = responseObject[i][@"title"];
        s.artist = responseObject[i][@"artist"];
        s.duration = responseObject[i][@"duration"];
        s.thumbURL = responseObject[i][@"thumb_url"];
        s.lyrics = responseObject[i][@"lyrics"];
        
        [newSongs addObject:s];
    }
    
    _music = [NSArray arrayWithArray:newSongs];
}

#pragma mark - UITableViewDataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _music.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"protoCell"];
    
    Song *song = _music[indexPath.row];
    
    cell.textLabel.text = song.artist;
    cell.detailTextLabel.text = song.title;
    
    NSURL *imageURL = [NSURL URLWithString:song.thumbURL];
    [cell.imageView setImageWithURL:imageURL];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"MainToDetail" sender:_music[indexPath.row]];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    DetailViewController *dvc = segue.destinationViewController;
    dvc.song = sender;
    
}

- (void)postExample {

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSDictionary *params = @{
                             @"nome": @"Helder",
                             @"valor": @(34),
                             @"texto1": @"la ala la la l al al a"
                             };

    NSString *urlString = @"http://reality6.com/musicservicejson.php";
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:params error:nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"DADOS ENVIADOS COM SUCESSO...");
        }
    }];
    [dataTask resume];
    


}

@end
