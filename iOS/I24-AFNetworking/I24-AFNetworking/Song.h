//
//  Song.h
//  I24-AFNetworking
//
//  Created by Helder Pereira on 31/08/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

@property (strong, nonatomic) NSNumber *webID;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *artist;
@property (strong, nonatomic) NSString *duration;
@property (strong, nonatomic) NSString *thumbURL;
@property (strong, nonatomic) NSString *lyrics;

@end
