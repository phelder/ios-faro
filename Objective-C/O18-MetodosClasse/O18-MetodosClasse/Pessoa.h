//
//  Pessoa.h
//  O18-MetodosClasse
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (nonatomic) int idade;
@property (strong, nonatomic) NSString *cidade;

- (instancetype)initWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade;

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade;

- (void)teste;
+ (void)metodoDaClasse;

@end
