//
//  main.m
//  O18-MetodosClasse
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    Pessoa *p2 = [Pessoa new];
    Pessoa *p3 = [[Pessoa alloc] initWithNome:@"H" idade:12 cidade:@"F"];
    Pessoa *p4 = [Pessoa pessoaWithNome:@"X" idade:15 cidade:@"L"];
    
    [p1 teste];
    [Pessoa metodoDaClasse];
    
    NSString *teste = [[NSString alloc] initWithFormat:@"%@ - %d", @"Olá", 12];
    NSString *t2 = [NSString stringWithFormat:@"%@ - %d", @"Adeus", 24];
    
    return 0;
}
