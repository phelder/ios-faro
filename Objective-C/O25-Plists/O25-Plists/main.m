//
//  main.m
//  O25-Plists
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSArray<NSNumber *> *numeros = @[
                                     @(23),
                                     @(43),
                                     @(23),
                                     @(1),
                                     @(12),
                                     @(76),
                                     @(35),
                                     @(94),
                                     @(2000)
                                     ];
    
//    NSDictionary *coisas = @{
//                             @"nome": @"Tobias",
//                             @"idade": @(20),
//                             @"morada": @"Tal e tal...",
//                             @"cores": @[
//                                     @"amarelo",
//                                     @"azul",
//                                     @"verde",
//                                     @"vermelho"
//                                     ]
//                             };
//    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *desktopURL = [fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *newFile = [desktopURL URLByAppendingPathComponent:@"teste/fx1.plist"];
    
    //[coisas writeToURL:newFile atomically:YES];
    
    NSDictionary *lido = [NSDictionary dictionaryWithContentsOfURL:newFile];
    
    NSLog(@"%@", lido);
    
    return 0;
}
