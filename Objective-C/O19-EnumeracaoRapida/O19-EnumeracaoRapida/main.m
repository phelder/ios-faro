//
//  main.m
//  O19-EnumeracaoRapida
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray <Pessoa *> *gente = @[
                                  [Pessoa pessoaWithNome:@"A" idade:10 cidade:@"Porto"],
                                  [Pessoa pessoaWithNome:@"B" idade:11 cidade:@"Faro"],
                                  [Pessoa pessoaWithNome:@"C" idade:6 cidade:@"Porto"],
                                  [Pessoa pessoaWithNome:@"D" idade:20 cidade:@"Faro"],
                                  [Pessoa pessoaWithNome:@"E" idade:34 cidade:@"Lisboa"],
                                  [Pessoa pessoaWithNome:@"F" idade:23 cidade:@"Faro"],
                                  [Pessoa pessoaWithNome:@"G" idade:26 cidade:@"Porto"],
                                  [Pessoa pessoaWithNome:@"H" idade:29 cidade:@"Faro"],
                                  [Pessoa pessoaWithNome:@"I" idade:45 cidade:@"Lisboa"],
                                  [Pessoa pessoaWithNome:@"J" idade:50 cidade:@"Lisboa"],
                                  [Pessoa pessoaWithNome:@"K" idade:5 cidade:@"Faro"]
                                  ];
    
    for (Pessoa *p in gente) {
        
        NSLog(@"%@ - %d - %@", p.nome, p.idade, p.cidade);
        
    }
    
    return 0;
}
