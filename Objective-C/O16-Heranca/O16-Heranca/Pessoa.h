//
//  Pessoa.h
//  O16-Heranca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (nonatomic) int idade;
@property (strong, nonatomic) NSString *cidade;

- (void)fazCoisas;

@end
