//
//  main.m
//  O16-Heranca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    
    p1.nome = @"Zé";
    p1.idade = 20;
    p1.cidade = @"Porto";
    
    Aluno *a1 = [[Aluno alloc] init];
    
    a1.nome = @"Tobias";
    a1.idade = 12;
    a1.cidade = @"Faro";
    a1.turma = @"A";
    
    return 0;
}
