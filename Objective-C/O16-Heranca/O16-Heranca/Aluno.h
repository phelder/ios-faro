//
//  Aluno.h
//  O16-Heranca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@interface Aluno : Pessoa

@property (strong, nonatomic) NSString *turma;

@end
