//
//  main.m
//  O24-InputToFile
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSMutableString *frases = [[NSMutableString alloc] init];
    
    for (int i = 0; i < 5; i++) {
        char input[255];
        NSLog(@"Introduza uma frase:");
        fgets(input, 255, stdin);
        
        // input[strlen(input) -1] = '\0'; // APAGA O CARACTER \n DEIXADO PELO ENTER
        
        NSString *frase = [NSString stringWithCString:input encoding:NSUTF8StringEncoding];
        
        [frases appendString:frase];
    }
    
    NSLog(@"%@", frases);
    
    // ENCONTRAR O DESKTOP
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *desktopURL = [fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask][0];
    
    // CRIAR A PASTA, (SO CRIA CASO NAO EXISTA)
    
    NSURL *newFolder = [desktopURL URLByAppendingPathComponent:@"frases"];
    
    [fileManager createDirectoryAtURL:newFolder withIntermediateDirectories:YES attributes:nil error:nil];
    
    // DAR UM NOME AO FICHEIRO NA PASTA CRIADA
    
    NSURL *newFile = [newFolder URLByAppendingPathComponent:@"nomes.txt"];
    
    
    // ESCREVER O CONTEUDO NO FICHEIRO
    
    [frases writeToURL:newFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    
    return 0;
}
