//
//  main.m
//  O22-WritingTextFiles
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    
//    NSString *texto1 = @"Um texto qualquer";
//    
//    [texto1 writeToFile:@"/users/helder/desktop/teste/qq.txt" atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    NSError *error;
    
    NSString *textoLido = [NSString stringWithContentsOfFile:@"/users/helder/desktop/teste/qq.txt" encoding:NSUTF8StringEncoding error:&error];
    
    if (error != nil) {
        NSLog(@"%@", error);
    }
    
    NSLog(@"%@", textoLido);
    
    return 0;
}
