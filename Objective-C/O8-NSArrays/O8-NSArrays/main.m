//
//  main.m
//  O8-NSArrays
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    //NSArray *nomes = [[NSArray alloc] initWithObjects:@"Tobias", @"Tomé", nil];
    
    NSArray *nomes = @[
                       @"Tobias",
                       @"Tomé",
                       @"Tomás",
                       @"Tiago",
                       @"Bruno",
                       @"Ruca"
                       ];
    
    for (int i = 0; i < nomes.count; i++) {
    
        //NSLog(@"%d: %@", i, [nomes objectAtIndex:i]);
        
        NSLog(@"%d: %@", i, nomes[i]);
    }
    
    NSNumber *n1 = [[NSNumber alloc] initWithInt:23];
    
    NSArray <NSNumber *> *numeros = @[
                         n1,
                         [[NSNumber alloc] initWithInt:54],
                         @(34),
                         @(12),
                         @(40),
                         @(30),
                         @(20),
                         @(10)
                         ];
    
    for (int i = 0; i < numeros.count; i++) {
    
//        NSNumber *n = numeros[i];
//        int numero = n.intValue;
        
        int numero = ((NSNumber *)numeros[i]).intValue;
        
        int numero2 = numeros[i].intValue;
        int numero3 = [[numeros objectAtIndex:i] intValue];
        
    }
    
    
    
    NSArray<NSNumber *> *valores = @[
                         @(5),
                         @(6),
                         @(10),
                         @(12),
                         @(4),
                         @(20),
                         @(10),
                         @(20),
                         @(20),
                         @(10)
                         ];
    
    
    int max = valores[0].intValue;
    int min = valores[0].intValue;
    int soma = 0;
    for (int i = 0; i < valores.count; i++) {
        //soma = soma + valores[i].intValue;
        soma += valores[i].intValue;
        
        if (valores[i].intValue > max) {
            max = valores[i].intValue;
        }
        
        if (valores[i].intValue < min) {
            min = valores[i].intValue;
        }
    }
    NSLog(@"Max: %d", max);
    NSLog(@"Min: %d", min);
    NSLog(@"Soma: %d", soma);
    NSLog(@"Média: %f", soma / (float)valores.count);
    
    
    return 0;
}
