//
//  main.m
//  O13-Palindromo
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSString *texto = @"óvo";
    
    
    
    NSMutableString *textoInvertido = [[NSMutableString alloc] init];
    
    for (int i = 0; i < texto.length; i++) {
    
        //char letra = [texto characterAtIndex:texto.length -i -1];
        //[textoInvertido appendFormat:@"%c", letra];
        
        NSString *strLetra = [texto substringWithRange:NSMakeRange(texto.length -i -1, 1)];
        
        [textoInvertido appendString:strLetra];
        
    }
    
    NSLog(@"%@", texto);
    NSLog(@"%@", textoInvertido);
    
    if ([texto compare:textoInvertido options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch] == NSOrderedSame) {
        NSLog(@"PALINDROMO");
    } else {
        NSLog(@"NAO");
    }
    
    return 0;
}
