//
//  Pessoa.m
//  O18-MetodosClasse
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)initWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade
{
    self = [super init];
    if (self) {
        _nome = nome;
        _idade = idade;
        _cidade = cidade;
    }
    return self;
}

+ (instancetype)pessoaWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade {
    
    return [[Pessoa alloc] initWithNome:nome idade:idade cidade:cidade];
    
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@ - %d %@", _nome, _idade, _cidade];

}

@end
