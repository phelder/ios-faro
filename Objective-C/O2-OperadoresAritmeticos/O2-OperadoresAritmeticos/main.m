//
//  main.m
//  O2-OperadoresAritmeticos
//
//  Created by Helder Pereira on 13/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    int num1 = 12;
    int num2 = 5;
    
    //     +    -    *    /     %
    int soma = num1 + num2;
    int sub = num1 - num2;
    int multi = num1 * num2;
    float divi = (float)num1 / num2;
    int resto = num1 % num2;
    
    
    NSLog(@"A soma é: %d", soma);
    NSLog(@"A subtração é: %d", sub);
    NSLog(@"A multiplicação é: %d", multi);
    NSLog(@"A divisão é: %f", divi);
    NSLog(@"A resto é: %d", resto);
    
    return 0;
}
