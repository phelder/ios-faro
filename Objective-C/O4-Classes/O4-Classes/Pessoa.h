//
//  Pessoa.h
//  O4-Classes
//
//  Created by Helder Pereira on 13/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

// public class Pessoa extends Object
@interface Pessoa : NSObject

@property NSString *nome;
@property int idade;
@property NSString *cidade;

@end
