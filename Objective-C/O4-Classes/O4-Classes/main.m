//
//  main.m
//  O4-Classes
//
//  Created by Helder Pereira on 13/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    // APP ARRANCA
    // VOU CRIAR 1 PESSOA
    
    // Pessoa p1 = new Pessoa();
    Pessoa *p1 = [[Pessoa alloc] init];
    p1.nome = @"Helder";
    p1.cidade = @"Porto";
    p1.idade = 35;
    
    Pessoa *p2 = [[Pessoa alloc] init];
    p2.nome = @"Tobias";
    p2.cidade = @"Faro";
    p2.idade = 30;
    
    
    if (p1.idade > p2.idade) {
    
        NSLog(@"O mais velho é o %@", p1.nome);
        
    } else if (p1.idade < p2.idade) {
    
        NSLog(@"O mais velho é o %@", p2.nome);
        
    } else {
        
        NSLog(@"Sao iguais...");
        
    }
    
    
    return 0;
}
