//
//  main.m
//  O14-ConsoleInput
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    
    
    
    // JOGO FORCA
    // FraseSecreta
    NSString *fraseSecreta = @"Qualquer coisa";
    NSMutableString *fraseJogo = [[NSMutableString alloc] init];
    
    for (int i = 0; i < fraseSecreta.length; i++) {
        
        char letra = [fraseSecreta characterAtIndex:i];
        if (letra == ' ') {
            [fraseJogo appendString:@" "];
        } else {
            [fraseJogo appendString:@"-"];
        }
        
    }
    
    NSLog(@"%@", fraseJogo);
    
    
    int vidas = 6;
    
    while (vidas > 0) {
        
        // VAMOS PEDIR UMA LETRA
        // E VERIFICAR SE ELA EXISTE
        
        char input[10];
        NSLog(@"Introduza uma letra:");
        fgets(input, 10, stdin);
        input[strlen(input) -1] = '\0'; // APAGA O CARACTER \n DEIXADO PELO ENTER
        
        NSString *letraUtilizador = [[NSString alloc] initWithCString:input encoding:NSUTF8StringEncoding];
        
        BOOL acertou = NO;
        
        for (int i = 0; i < fraseSecreta.length; i++) {
            
            NSRange range = NSMakeRange(i, 1);
            
            NSString *letraFrase = [fraseSecreta substringWithRange:range];
            
            if ([letraFrase compare:letraUtilizador options:NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch] == NSOrderedSame) {
                
                [fraseJogo replaceCharactersInRange:range withString:letraFrase];
                acertou = YES;
            }
            
        }
        
        if (!acertou) {
            vidas--;
        }
        
        
        NSLog(@"%@", fraseJogo);
    }
    
   
    
    return 0;
}
