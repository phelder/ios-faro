//
//  main.m
//  O23-NSFileManager
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray<NSURL *> *urls = [fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask];
    
    NSURL *desktopURL = urls[0];
    
    NSLog(@"%@", desktopURL.scheme);
    
    NSURL *newFile = [desktopURL URLByAppendingPathComponent:@"teste/outro.txt"];
    
    NSLog(@"%@", newFile);
    
    [@"mais um teste\noutra linha\n\n\n\n\n\nmais abaixo" writeToURL:newFile atomically:YES encoding:NSUTF8StringEncoding error:nil];
    
    return 0;
}
