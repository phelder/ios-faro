//
//  main.m
//  O3-Ciclos
//
//  Created by Helder Pereira on 13/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    int numero = 7;
    
    // 7 X 1 = 7
    // 7 X 2 = 14
    // 7 X 3 = 21
    // ...
    // 7 X 10 = 70
    
    for (int i = 1; i <= 10; i++) {
    
        int resultado = numero * i;
        
        NSLog(@"%d X %d = %d", numero, i, resultado);
    
    }
    
    
    return 0;
}
