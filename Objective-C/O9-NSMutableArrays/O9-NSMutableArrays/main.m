//
//  main.m
//  O9-NSMutableArrays
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int randomiza(int min, int max) {
    
    return (arc4random() % max - min + 1) + min;
    
}

int main(int argc, const char * argv[]) {
    
    NSMutableArray<NSString *> *malta = [[NSMutableArray alloc] init];
    
    [malta addObject:@"Tobias"];
    [malta addObject:@"Tomé"];
    [malta addObject:@"Tomás"];
    [malta addObject:@"Tiago"];
    
    NSLog(@"%@", malta);
    
    [malta removeObjectAtIndex:2];
    
    for (int i = 0; i < malta.count; i++) {
    
        NSLog(@"%d: %@", i, malta[i]);
        
    }
    
    
    NSMutableArray<NSNumber *> *numeros = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 10; i++) {
    
        int n;
        do {
            
            n = randomiza(1, 10);
            
        } while ([numeros containsObject:@(n)]);
        
        [numeros addObject:@(n)];
    }
    
    NSLog(@"%@", numeros);
    
    return 0;
}
