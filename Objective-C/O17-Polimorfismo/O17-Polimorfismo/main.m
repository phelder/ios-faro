//
//  main.m
//  O17-Polimorfismo
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Aluno.h"

int main(int argc, const char * argv[]) {
    
    
    Pessoa *p1 = [[Aluno alloc] init]; // #12345
    
    p1.nome = @"T";
    p1.idade = 12;
    p1.cidade = @"Faro";
   
    
//    Aluno *a1 = (Aluno *)p1;
//    a1.turma = @"A";
    
    ((Aluno *)p1).turma = @"A";

    
    NSArray<Pessoa *> *malta = @[
                                 [[Pessoa alloc] init],
                                 [[Aluno alloc] init],
                                 [[Aluno alloc] init],
                                 [[Pessoa alloc] init],
                                 [[Aluno alloc] init],
                                 [[Aluno alloc] init],
                                 [[Aluno alloc] init],
                                 [[Pessoa alloc] init],
                                 [[Aluno alloc] init],
                                 [[Aluno alloc] init]
                                 ];
    
    for (int i = 0; i < malta.count; i++) {
        
        if ([malta[i] isKindOfClass:[Aluno class]]) {
            NSLog(@"%d: ALUNO", i);
        } else {
            NSLog(@"%d: NAO E ALUNO", i);
        }
    
    }
    
    return 0;
}
