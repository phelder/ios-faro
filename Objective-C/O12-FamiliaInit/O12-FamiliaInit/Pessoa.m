//
//  Pessoa.m
//  O12-FamiliaInit
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _nome = @"Nome por defeito";
        _cidade = @"Cidade por defeito";
        _idade = 0;
        
    }
    return self;
}

- (instancetype)initWithNome:(NSString *)nome idade:(int)idade cidade:(NSString *)cidade {
    self = [super init];
    if (self) {
        
        _nome = nome;
        _cidade = cidade;
        _idade = idade;
        
    }
    return self;
}

@end
