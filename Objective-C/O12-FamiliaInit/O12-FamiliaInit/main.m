//
//  main.m
//  O12-FamiliaInit
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    
    Pessoa *p1 = [[Pessoa alloc] init];
    p1.nome = @"A";
    p1.cidade = @"F";
    p1.idade = 10;
    
    
    NSLog(@"%@", p1.nome);
    
    Pessoa *p2 = [[Pessoa alloc] initWithNome:@"Marta" idade:23 cidade:@"Porto"];
    
    return 0;
}
