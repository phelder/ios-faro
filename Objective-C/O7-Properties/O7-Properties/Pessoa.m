//
//  Pessoa.m
//  O7-Properties
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

@synthesize nome = _nome;

- (void)qualquer {
    _nome = @"Teste";
    _idade = 23;
    _cidade = @"lalala";
}

@end
