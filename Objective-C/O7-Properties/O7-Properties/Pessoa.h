//
//  Pessoa.h
//  O7-Properties
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (readonly) NSString *nome;
@property (readonly) int idade;
@property NSString *cidade;

@end
