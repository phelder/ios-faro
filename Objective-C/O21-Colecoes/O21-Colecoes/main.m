//
//  main.m
//  O21-Colecoes
//
//  Created by Helder Pereira on 22/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
//    NSArray;
//    NSMutableArray;
//    
//    NSDictionary;
//    NSMutableDictionary;
//    
//    NSSet;
//    NSMutableSet;
//    
//    NSOrderedSet;
//    NSMutableOrderedSet;

    
    NSDictionary *dict1 = @{
                            @"nome": @(1),
                            @"morada": @(2),
                            @"genero": @(3),
                            @"telefone": @"Olá",
                            @"email": @"Adeus",
                            @"cod_postal": @"Talvez..."
                            };
    
    for (NSString *key in dict1) {
        
        //[dict1 objectForKey:key];
        NSLog(@"\nCHAVE: %@\nVALOR:%@\n\n", key, dict1[key]);
        
    }
    
    NSArray<NSString *> *keys = [dict1.allKeys sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", keys);
    
    for (int i = 0; i < keys.count; i++) {
        NSString *key = keys[i];
        
        NSLog(@"\nCHAVE: %@\nVALOR:%@\n\n", key, dict1[key]);
    }
    
    return 0;
}
