//
//  Pessoa.h
//  P26-Memoria
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property NSString *nome; // strong, atomic, readwrite
@property (strong, nonatomic) NSString *cidade;
@property (assign, nonatomic) int idade; //assign, atomic, readwrite

@end
