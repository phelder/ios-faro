//
//  main.m
//  P26-Memoria
//
//  Created by Helder Pereira on 25/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {

    // ARC: Automatic Reference Counting
    
    NSMutableString *teste = [[NSMutableString alloc] init]; //ContadorApontadores :0
    
    Pessoa *p1 = [[Pessoa alloc] init];
    p1.nome = teste;
    
    teste = nil;
    
    return 0;
}
