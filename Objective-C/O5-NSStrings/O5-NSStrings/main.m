//
//  main.m
//  O5-NSStrings
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    
    
    NSString *nome1;
    
    nome1 = @"Tobias";
    
    // FORMA DE INVOCAR UM METODO EM JAVA:
    // int tamanhoString = nome1.length();
    
    // EM OBJECTIVE-C
    int tamanhoString = (int)[nome1 length];
    
    NSLog(@"O tamanho da string %@ é %d", nome1, tamanhoString);
    
    
    NSString *nome2 = @"tobIAS";
    
    if ([nome1 caseInsensitiveCompare:nome2] == NSOrderedSame) {
        
        NSLog(@"Os nomes são iguais");
    
    } else {
        
        NSLog(@"Os nomes são diferentes");
    }
    
    return 0;
}
