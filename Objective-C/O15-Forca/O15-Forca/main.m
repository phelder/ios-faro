//
//  main.m
//  O15-Forca
//
//  Created by Helder Pereira on 20/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Forca.h"

int main(int argc, const char * argv[]) {
    
    NSArray<NSString *> *frases = @[
                                    @"Batman",
                                    @"Superman",
                                    @"Aquaman",
                                    @"Wonder Woman",
                                    @"Green Lantern",
                                    @"Green Arrow",
                                    @"Robin",
                                    @"Nightwing"
                                    ];
    
    // LER CONTEUDO DE UM FICHEIRO
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSURL *desktopURL = [fileManager URLsForDirectory:NSDesktopDirectory inDomains:NSUserDomainMask][0];
    
    NSURL *url = [desktopURL URLByAppendingPathComponent:@"frases/nomes.txt"];
    
    NSString *stringFrases = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
   
    
    frases = [stringFrases componentsSeparatedByString:@"\n"];
    
    NSLog(@"%@", frases);
    
    Forca *f1 = [[Forca alloc] initWithFrases:frases];
    
    while ([f1 emJogo]) {
        
        NSLog(@"Frase: %@", f1.fraseJogo);
        NSLog(@"Vidas: %d", f1.vidas);
        
        char input[10];
        NSLog(@"Introduza uma letra:");
        fgets(input, 10, stdin);
        input[strlen(input) -1] = '\0'; // APAGA O CARACTER \n DEIXADO PELO ENTER
        NSString *letraUtilizador = [[NSString alloc] initWithCString:input encoding:NSUTF8StringEncoding];
        
        if ([f1 verificaLetra:letraUtilizador]) {
            NSLog(@"ACERTOU A LETRA");
        } else {
            NSLog(@"FALHOU A LETRA");
        }
        
    }
    
    if ([f1 venceu]) {
        NSLog(@"PARABENS!!!");
    } else {
        NSLog(@"NABO...");
    }
    
    
    return 0;
}
