//
//  Pessoa.m
//  O6-Classes2
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa
// ivars
{
    NSString *_nome;
    int _idade;
    NSString *_cidade;
}

// public String getNome() { ... }
- (NSString *)nome {
    return _nome;
}

// public void setNome(String nome) { ... }
- (void)setNome:(NSString *)nome {
    _nome = nome;
}


- (int)idade {
    return _idade;
}


- (void)setIdade:(int)idade {
    _idade = idade;
}

// public String getNome() { ... }
- (NSString *)cidade {
    
    NSLog(@"EU SOU A PROVA QUE A DOT SYNTAX E UM ATALHO PATA OS GETTERS E SETTERS...");
    
    return _cidade;
}

// public void setNome(String nome) { ... }
- (void)setCidade:(NSString *)cidade {
    _cidade = cidade;
}

// JS ou PHP:
//function teste() { ... }

// JAVA:
//public void teste() { ... }

- (void)teste {
    
    NSLog(@"Isto é um teste");
    
}

@end
