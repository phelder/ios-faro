//
//  main.m
//  O6-Classes2
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    Pessoa *p1 = [[Pessoa alloc] init];
    
    [p1 teste];
    
    [p1 setNome:@"Tobias"];
    [p1 setIdade:20];
    [p1 setCidade:@"Faro"];
    
    Pessoa *p2 = [[Pessoa alloc] init];
    
    p2.nome = @"Tomé";
    p2.idade = 22;
    p2.cidade = @"Faro"; // [p2 setCidade:@"Faro"];
    
    int idade = p2.idade; // [p2 idade];
    
    NSLog(@"O nome é %@ e a idade é %d", p2.nome, idade);
    
    NSString *c = p2.cidade;
    
    return 0;
}
