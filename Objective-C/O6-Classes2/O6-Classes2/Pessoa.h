//
//  Pessoa.h
//  O6-Classes2
//
//  Created by Helder Pereira on 15/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

- (NSString *)nome;
- (void)setNome:(NSString *)nome;

- (int)idade;
- (void)setIdade:(int)idade;

- (NSString *)cidade;
- (void)setCidade:(NSString *)cidade;

- (void)teste;

@end
