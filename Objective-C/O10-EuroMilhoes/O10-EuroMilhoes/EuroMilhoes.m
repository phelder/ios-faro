//
//  EuroMilhoes.m
//  O10-EuroMilhoes
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "EuroMilhoes.h"

@implementation EuroMilhoes

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self geraChave];
    }
    return self;
}

// randomizaEntre:e:
- (int)randomizaEntre:(int)min e:(int)max
{
    return (arc4random() % max - min + 1) + min;
}

- (void)geraChave
{
    NSMutableArray<NSNumber *> *numeros = [[NSMutableArray alloc] init];
    NSMutableArray<NSNumber *> *estrelas = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 5; i++) {
        int n;
        do {
            n = [self randomizaEntre:1 e:50];
        } while ([numeros containsObject:@(n)]);
        [numeros addObject:@(n)];
    }
    
    for (int i = 0; i < 2; i++) {
        int n;
        do {
            n = [self randomizaEntre:1 e:11];
        } while ([estrelas containsObject:@(n)]);
        [estrelas addObject:@(n)];
    }
    
    _numeros = [numeros sortedArrayUsingSelector:@selector(compare:)];
    _estrelas = [estrelas sortedArrayUsingSelector:@selector(compare:)];
    
}

- (NSString *)numerosString {
    
//    NSMutableString *novaString = [[NSMutableString alloc] init];
//    
//    for (int i = 0; i < _numeros.count; i++) {
//        if (i != 0) {
//            [novaString appendString:@", "];
//        }
//        [novaString appendFormat:@"%@", _numeros[i]];
//    }
//    return novaString.description;
    
    return [_numeros componentsJoinedByString:@", "];
}

- (NSString *)estrelasString {
    return [_estrelas componentsJoinedByString:@", "];
}

- (NSString *)chave {
    
    return [[NSString alloc] initWithFormat:@"%@ - %@", self.numerosString, self.estrelasString];
    
}

- (NSString *)description {
    
    return self.chave;
    
}

- (NSComparisonResult)compare:(EuroMilhoes *)other {
    
    return [self.chave compare:other.chave];
    
}


@end
