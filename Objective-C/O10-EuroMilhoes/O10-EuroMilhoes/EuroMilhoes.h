//
//  EuroMilhoes.h
//  O10-EuroMilhoes
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EuroMilhoes : NSObject

@property (strong, nonatomic, readonly) NSArray<NSNumber *> *numeros;
@property (strong, nonatomic, readonly) NSArray<NSNumber *> *estrelas;

- (NSString *)numerosString;  // 1, 4, 5, 12, 34
- (NSString *)estrelasString; // 2, 3
- (NSString *)chave;          // 1, 4, 5, 12, 34 - 2, 3

- (NSComparisonResult)compare:(EuroMilhoes *)other;

@end
