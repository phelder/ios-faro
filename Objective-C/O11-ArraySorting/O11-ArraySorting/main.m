//
//  main.m
//  O11-ArraySorting
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pessoa.h"

int main(int argc, const char * argv[]) {
    
    NSArray *array1 = @[
                        @(10),
                        @(4),
                        @(23),
                        @(43),
                        @(1),
                        @(5),
                        @(-6)
                        ];
    
    NSArray *array2 = [array1 sortedArrayUsingSelector:@selector(compare:)];
    
    NSLog(@"%@", array2);
    
    
    
    Pessoa *p1 = [[Pessoa alloc] init];
    Pessoa *p2 = [[Pessoa alloc] init];
    Pessoa *p3 = [[Pessoa alloc] init];
    Pessoa *p4 = [[Pessoa alloc] init];
    
    p1.nome = @"Tobias";
    p1.idade = 23;
    p1.cidade = @"Faro";
    
    p2.nome = @"Marco";
    p2.idade = 30;
    p2.cidade = @"Aveiro";
    
    p3.nome = @"Zé";
    p3.idade = 24;
    p3.cidade = @"Lisboa";
    
    p4.nome = @"Américo";
    p4.idade = 10;
    p4.cidade = @"Faro";
    
    NSArray<Pessoa *> *gente = @[p1, p2, p3, p4];
   
    NSLog(@"%@", gente);
    
    
    // NSARRAY ORDENADO COM SELECTOR COMPARE CRIADO DENTRO DA CLASSE PESSOA
    NSArray<Pessoa *> *genteOrdenada = [gente sortedArrayUsingSelector:@selector(comparaPorIdade:)];
    
    NSLog(@"%@", genteOrdenada);
    
    return 0;
}
