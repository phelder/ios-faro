//
//  Pessoa.m
//  O11-ArraySorting
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import "Pessoa.h"

@implementation Pessoa

- (NSComparisonResult)comparaPorNome:(Pessoa *)outraPessoa
{
    
    return [self.nome compare:outraPessoa.nome];
    
}

- (NSComparisonResult)comparaPorIdade:(Pessoa *)outraPessoa
{

//    if (self.idade == outraPessoa.idade) {
//        return NSOrderedSame;
//    } else if (self.idade > outraPessoa.idade) {
//        return NSOrderedDescending;
//    } else {
//        return NSOrderedAscending;
//    }
    
    return [@(self.idade) compare:@(outraPessoa.idade)];
    
}

- (NSComparisonResult)comparaPorCidade:(Pessoa *)outraPessoa
{
    
    return [self.cidade compare:outraPessoa.cidade];
    
}

- (NSString *)description {
    
    return [[NSString alloc] initWithFormat:@"%@ - %d - %@", _nome, _idade, _cidade];
    
}

@end
