//
//  Pessoa.h
//  O11-ArraySorting
//
//  Created by Helder Pereira on 18/07/16.
//  Copyright © 2016 Helder Pereira. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pessoa : NSObject

@property (strong, nonatomic) NSString *nome;
@property (nonatomic) int idade;
@property (strong, nonatomic) NSString *cidade;

- (NSComparisonResult)comparaPorNome:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorIdade:(Pessoa *)outraPessoa;
- (NSComparisonResult)comparaPorCidade:(Pessoa *)outraPessoa;

@end
